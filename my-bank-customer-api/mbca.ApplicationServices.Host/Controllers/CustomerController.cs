﻿using Microsoft.AspNetCore.Mvc;
using mbca.DomainServices.Providers;
using mbca.DomainServices.Services;

namespace mbca.ApplicationServices.Host.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerProvider provider;
        private readonly ILogger<CustomerController> logger;
        private const string LOGHDR = "CustomerController :: Get() Method :: Exception :: {0} ";

        public CustomerController(ICustomerProvider provider, ILogger<CustomerController> logger)
        {
            this.provider = provider;
            this.logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("api/Customer/get/{id}")]
        [HttpGet]
        public IActionResult Get(string id) {
            logger.LogInformation(string.Format(LOGHDR, id));

            Guid guid;

            if (string.IsNullOrEmpty(id) || !Guid.TryParse(id, out guid))
            {
                logger.LogError(string.Format(LOGHDR, "Methods argument exception"));
                throw new ApplicationException(LOGHDR);
            }

            try
            {
                var customer = provider.GetCustomer(guid);
                return View(customer);
            }
            catch (Exception e)
            {
                throw new ApplicationException(LOGHDR);
            }
        }


        [Route("api/Customer/get")]
        [HttpGet]
        public IActionResult GetAll() {
            try
            {
                var customers = provider.GetCustomers();
                return View(customers);
            }
            catch(Exception e)
            {
                throw new ApplicationException(string.Format(LOGHDR, e.StackTrace.ToString()));
            }
        }

    }
}
