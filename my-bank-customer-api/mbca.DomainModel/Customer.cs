﻿namespace mbca.DomainModel
{
    public record Customer
    {
        public Customer() { }

        public Customer(Guid id, string name, string surname, string address, string city, string province, string country)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.address = address;
            this.city = city;
            this.province = province;
            this.country = country;
        }

        public Guid id { get; private set; }
        public string name { get; private set; }
        public string surname { get; private set; }
        public string address { get; private set; }
        public string city { get; private set; }
        public string province { get; private set; }
        public string country { get; private set; }
    }
}