﻿using mbca.DomainModel;

namespace mbca.DomainServices.Mappers
{
    public interface IMapper
    {
        public Customer Map(string message);
    }
}
