﻿using mbca.DomainModel;

namespace mbca.DomainServices.Providers
{
    public interface ICustomerProvider
    {
        IList<Customer> GetCustomers();
        Customer GetCustomer(Guid guid);
    }
}
