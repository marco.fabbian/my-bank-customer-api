﻿using mbca.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mbca.DomainServices.Services
{
    public interface ICustomerRepository
    {
        public IList<Customer> GetList();
        public Customer Get(Guid id);
    }
}
