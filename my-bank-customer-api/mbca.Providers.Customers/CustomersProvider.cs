﻿using mbca.DomainModel;
using mbca.DomainServices.Providers;
using mbca.DomainServices.Services;

namespace mbca.Providers.Customers
{
    public class CustomersProvider : ICustomerProvider
    {
        private readonly ICustomerRepository repo;

        public CustomersProvider(ICustomerRepository repo)
        {
            this.repo = repo;
        }

        public Customer GetCustomer(Guid guid)
        {
            return repo.Get(guid);
        }

        public IList<Customer> GetCustomers()
        {
            return repo.GetList();
        }
    }
}
