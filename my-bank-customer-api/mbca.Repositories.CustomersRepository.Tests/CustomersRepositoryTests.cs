﻿using FluentAssertions;
using mbca.DomainModel;
using mbca.DomainServices.Mappers;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace mbca.Repositories.Customers.Tests
{
    public class CustomersRepositoryTests
    {

        public CustomersRepositoryTests()
        {

        }

        [Test]
        public void ConsumeMessageFromTopic_ShouldSucceded() 
        {
            //Arrange
            var topicName = "com.marcofabbian.transaction.Customer";
            var config = new KafkaConfiguration()
            {
                BootstrapServers = "localhost:9092",
                GroupId = topicName + "::groupId",
                TopicName = topicName,
                SchemaRegistry = "localhost:8081"
            };
            var kafkaConfig = new Mock<IOptions<KafkaConfiguration>>();
            kafkaConfig.Setup(x => x.Value).Returns(config);

            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map(It.IsAny<string>())).Returns(new Customer());
            var target = new CustomersRepository(kafkaConfig.Object, mapper.Object);

            //Act
            var results = target.GetList();

            //Assert
            results.Should().NotBeEmpty();
        }
    }
}