﻿using Avro.Generic;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using mbca.DomainModel;
using mbca.DomainServices.Services;
using mbca.DomainServices.Mappers;
using Microsoft.Extensions.Options;

namespace mbca.Repositories.Customers
{
    public class CustomersRepository : ICustomerRepository
    {
        private readonly KafkaConfiguration kafkaConfg;
        private readonly IMapper mapper;
        private int CONST = 10000;

        public CustomersRepository(IOptions<KafkaConfiguration> kafkaConfg, IMapper mapper)
        {
            this.kafkaConfg = kafkaConfg.Value;
            this.mapper = mapper;
        }

        public Customer Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public IList<Customer> GetList()
        {
            Message<string, GenericRecord> message = null;
            var customers = new List<Customer>();
            while ((message = GetMessage()) != null)
            {
                customers.Add(mapper.Map(message.Value.ToString()));
            }

            return customers.ToArray();

        }

        public Message<string, GenericRecord> GetMessage()
        {
            var consumerConfig = new ConsumerConfig()
            {
                EnableSslCertificateVerification = false,
                BootstrapServers = kafkaConfg.BootstrapServers,
                SecurityProtocol = SecurityProtocol.Plaintext,
                //SslKeystoreLocation = "",
                //SslKeystorePassword = "",
                //SslCaLocation = "",
                GroupId = kafkaConfg.GroupId,
                EnableAutoCommit = true,
                EnableAutoOffsetStore = true,
                AutoOffsetReset = AutoOffsetReset.Earliest,
            };

            var schemaRegistryConfig = new SchemaRegistryConfig()
            {
                EnableSslCertificateVerification = false,
                BasicAuthCredentialsSource = AuthCredentialsSource.UserInfo,
                Url = kafkaConfg.SchemaRegistry,
            };

            // Construct the Schema Registry Client and serializer
            var srClient = new CachedSchemaRegistryClient(schemaRegistryConfig);
            var avroDeserializer = new AvroDeserializer<GenericRecord>(srClient).AsSyncOverAsync();


            using (var consumer = new ConsumerBuilder<string, GenericRecord>(consumerConfig)
                .SetValueDeserializer(avroDeserializer)
                .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Code} -{e.Reason} "))
                .Build())
            {

                consumer.Subscribe(kafkaConfg.TopicName);
                var cancelToken = new CancellationTokenSource();

                bool repeat = true;
                while (repeat)
                {
                    var result = consumer.Consume(TimeSpan.FromSeconds(3));

                    if (result == null)
                        continue;

                    if (result.IsPartitionEOF)
                        repeat = false;

                    try
                    {
                        if (result.Message.Value != null)
                        {
                            consumer.Commit(result);
                            return result.Message;
                        }
                    }
                    catch (Exception ke)
                    {
                        Console.WriteLine(ke.Message.ToString());
                    }
                }
                consumer.Close();
            }
            return null;
        }

    }
}