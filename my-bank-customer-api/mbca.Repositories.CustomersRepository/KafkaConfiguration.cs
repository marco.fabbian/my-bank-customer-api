﻿namespace mbca.Repositories.Customers
{
    public class KafkaConfiguration
    {
        public const string KafkaConfig = "KafkaConfiguration";

        public string BootstrapServers { get; set; }
        public string TopicName { get; set; }
        public string GroupId { get; set; }
        public string ClientId { get; set; }
        public string SchemaRegistry { get; set; }
    }
}
